FROM java:8-jdk-alpine
WORKDIR /
COPY ./target/*.jar /app.jar
COPY script.sh /script.sh
CMD ["-start"]
ENTRYPOINT ["sh", "/script.sh"]
EXPOSE 8090

